# Rev 3

On a un binaire exécutable 64bits stripped. En l'exécutant, il affiche 'hello!' et se termine.

Avec la commande `strings`, je comprends que le binaire a été packé avec UPX. En m'informant sur cet outil, j'apprends qu'il est possible de 'dépacker' avec `upx -d`, mais que ceci peut être déjoué par certaines mesures. Ça vaut quand même la peine d'essayer.
Après avoir 'dépacké' le binaire, il semble s'exécuter de la même manière. Un suivi en debug du programme ne révèle rien d'intéressant, un appel à `puts` dans `main `et c'est tout.

Toujours rien avec `strings`. Avec `objdump` je regarde d'éventuelles autres fonctions qui seraient intéressantes. Dans la fonction `x`, ça saute aux yeux, on met une chaine ascii sur le stack et elle a l'air de commencer par UQAM.

~~~
0000000000400abd <x>:
  400abd:       55                      push   rbp
  400abe:       48 89 e5                mov    rbp,rsp
  400ac1:       c6 45 e0 55             mov    BYTE PTR [rbp-0x20],0x55
  400ac5:       c6 45 e1 51             mov    BYTE PTR [rbp-0x1f],0x51
  400ac9:       c6 45 e2 41             mov    BYTE PTR [rbp-0x1e],0x41
  400acd:       c6 45 e3 4d             mov    BYTE PTR [rbp-0x1d],0x4d
  400ad1:       c6 45 e4 7b             mov    BYTE PTR [rbp-0x1c],0x7b
  400ad5:       c6 45 e5 48             mov    BYTE PTR [rbp-0x1b],0x48
  400ad9:       c6 45 e6 30             mov    BYTE PTR [rbp-0x1a],0x30
  400add:       c6 45 e7 50             mov    BYTE PTR [rbp-0x19],0x50
  400ae1:       c6 45 e8 33             mov    BYTE PTR [rbp-0x18],0x33
  400ae5:       c6 45 e9 5f             mov    BYTE PTR [rbp-0x17],0x5f
  400ae9:       c6 45 ea 59             mov    BYTE PTR [rbp-0x16],0x59
  400aed:       c6 45 eb 4f             mov    BYTE PTR [rbp-0x15],0x4f
  400af1:       c6 45 ec 55             mov    BYTE PTR [rbp-0x14],0x55
  400af5:       c6 45 ed 5f             mov    BYTE PTR [rbp-0x13],0x5f
  400af9:       c6 45 ee 4b             mov    BYTE PTR [rbp-0x12],0x4b
  400afd:       c6 45 ef 6e             mov    BYTE PTR [rbp-0x11],0x6e
  400b01:       c6 45 f0 65             mov    BYTE PTR [rbp-0x10],0x65
  400b05:       c6 45 f1 77             mov    BYTE PTR [rbp-0xf],0x77
  400b09:       c6 45 f2 5f             mov    BYTE PTR [rbp-0xe],0x5f
  400b0d:       c6 45 f3 55             mov    BYTE PTR [rbp-0xd],0x55
  400b11:       c6 45 f4 50             mov    BYTE PTR [rbp-0xc],0x50
  400b15:       c6 45 f5 58             mov    BYTE PTR [rbp-0xb],0x58
  400b19:       c6 45 f6 7d             mov    BYTE PTR [rbp-0xa],0x7d
  400b1d:       90                      nop
  400b1e:       5d                      pop    rbp
  400b1f:       c3                      ret
~~~

## Flag

UQAM{H0P3\_YOU\_Knew\_UPX}
