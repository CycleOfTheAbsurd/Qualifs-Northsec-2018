# Web 2

`http://165.227.39.129:8081/`

On arrive sur une page web PHP avec une citation de fob et un formulaire avec un textarea.
Ce qu'on rentre dans le textarea est envoyé par POST et est inséré dans un tag `<script>` dans le `<head>` de la page retournée.

On peut injecter des choses dans le body en envoyant un tag `script` fermant et du contenu. Les `<?` et `?>` en dehors de tags `scripts` sont remplacés par des commentaires HTML. Il y a peut-être possibilité d'injection PHP, mais je vois pas comment? C'est peut-être aussi une fausse piste..

Nouvelle approche: on `curl` la page pour voir si j'aurais rien manqué. Je reçois un gros bloc de javascript sale que j'ai jamais vu dans mon browser, même avec JS désactivé..
C'est seulement des `![()]+`, ça ressemble à un esolang ça. Un petite recherche sur les esolang de JS, me révèle que c'est du JSFuck. Avec l'outil [JSUnfuck](http://codertab.com/jsunfuck) j'arrive à comprendre ce qui se passe.

## Flag
UQAM{SO\_YOURE\_A\_H4X0R?}
